#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import itertools
import logging
import random
import re
import sys
from collections import defaultdict, Counter
from functools import reduce
from operator import and_
from typing import NamedTuple


class Wordle:
    def __init__(self, words: list):
        self.word = random.choice(words)

    def check(self, word: str):
        hints = ''
        for i in range(0, 5):
            if word[i] == self.word[i]:
                hints += '2'
            elif word[i] not in self.word:
                hints += '0'
            else:
                found = False
                for j in range(0, 5):
                    if i != j and word[j] != self.word[j] and word[i] == self.word[j]:
                        found = True
                hints += '1' if found else '0'
        return True if hints == '22222' else False, hints


class WordHints(NamedTuple):
    word: str
    hints: str


class WordFilter:
    def __init__(self):
        self.forbidden = defaultdict(str)
        self.allowed = set()
        self.match = defaultdict(str)

    def exclude(self, char: str, index: int | None = None) -> None:
        if index is None:
            for i in range(0, 5):
                self.forbidden[i] += char
        else:
            self.forbidden[index] += char
            self.allowed.add(char)

    def right_guess(self, char: str, index: int) -> None:
        self.match[index] = char
        self.allowed.add(char)

    def filter_words(self, words: list) -> list:
        def _generate(index: int) -> str:
            if self.match[index]:
                return self.match[index]
            elif self.forbidden[index]:
                return f'[^{self.forbidden[index]}]'
            else:
                return '.'
        if self.allowed:
            words = list(filter(lambda x: reduce(and_, map(x.__contains__, self.allowed)), words))
        pattern = ''
        for i in range(0, 5):
            pattern += _generate(i)
        return list(filter(lambda x: re.fullmatch(pattern, x), words))


class WordleGuesser:
    def __init__(self, words: list):
        self.words = words
        self.word_filter = WordFilter()

    def _update(self, word_hints: WordHints):
        for i, c, x in zip(itertools.count(), word_hints.word, word_hints.hints):
            if x == '0':
                self.word_filter.exclude(char=c)
            elif x == '1':
                self.word_filter.exclude(char=c, index=i)
            elif x == '2':
                self.word_filter.right_guess(char=c, index=i)
        self.words = self.word_filter.filter_words(self.words)
        assert len(self.words) >= 1

    def guess(self, previous: WordHints | None):
        if previous:
            self._update(word_hints=previous)
        words = list(filter(lambda x: len(set(x)) == len(x), self.words))
        if words:
            return random.choice(self.words)
        return random.choice(self.words)


def run_game(words: list) -> tuple[bool, int]:
    wordle = Wordle(words=words)
    wordle_guesser = WordleGuesser(words=words)
    word_hints = None
    for i in range(0, 6):
        word = wordle_guesser.guess(previous=word_hints)
        win, hints = wordle.check(word=word)
        logging.debug(f'{word} ({hints})')
        if win:
            logging.info(f'Game won - {word}')
            return True, i + 1
        word_hints = WordHints(word=word, hints=hints)
    else:
        logging.info(f'Game lost - The solution would have been {wordle.word!r}.')
        return False, 0


def main():
    logging.basicConfig(stream=sys.stdout, level=logging.WARNING, format='%(message)s')

    # load word list
    with open('wordlist_german.txt', 'r') as file:
        words = [line.strip() for line in file]
    words = list(filter(lambda x: re.fullmatch(r'[a-z]{5}', x), words))

    # run games
    results = []
    max_rounds = 1000
    for i in range(0, max_rounds):
        results.append(run_game(words=words.copy()))
        print(
            f'\rGame {i: 5}/{max_rounds}',
            end=' ', flush=True)

    # print summary
    wins_loses = Counter(x[0] for x in results)
    print(f'\rGames  {max_rounds}')
    print(f'Wins:  {wins_loses[True] * 100 / max_rounds:.02f}%')
    print(f'Loses: {wins_loses[False] * 100 / max_rounds:.02f}%')
    histogram = Counter(x[1] for x in results)
    print('Distribution:')
    for i in range(1, 7):
        print(f'{i}: {histogram.get(i, 0)}')


if __name__ == '__main__':
    main()
